package com.nosequemapasa.hola;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.nosequemapasa.hola.repositories.MovieRepository;
import com.nosequemapasa.hola.resolvers.MutationResolver;
import com.nosequemapasa.hola.resolvers.QueryResolver;


@SpringBootApplication
public class NosequemepasaApplication {

	public static void main(String[] args) {
		SpringApplication.run(NosequemepasaApplication.class, args);
	}


	@Bean
	public QueryResolver query(MovieRepository movieRepository) {
		return new QueryResolver(movieRepository);
	}

	@Bean
	public MutationResolver mutation(MovieRepository movieRepository) {
		return new MutationResolver(movieRepository);
	}

}
