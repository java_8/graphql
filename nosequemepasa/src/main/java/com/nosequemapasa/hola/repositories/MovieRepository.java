package com.nosequemapasa.hola.repositories;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nosequemapasa.hola.model.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>{

	
}
