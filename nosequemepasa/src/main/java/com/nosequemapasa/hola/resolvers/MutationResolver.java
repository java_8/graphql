package com.nosequemapasa.hola.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.nosequemapasa.hola.model.Movie;
import com.nosequemapasa.hola.repositories.MovieRepository;



public class MutationResolver implements GraphQLMutationResolver {
	
	MovieRepository movieRepository;
	
	
	
	public MutationResolver(MovieRepository movieRepository) {
		this.movieRepository = movieRepository;
	}

	public Movie createMovie(String title, String genre, String[] actors) {
		
		Movie movie = new Movie();
		movie.setTitle(title);
		movie.setGenre(genre);
		movie.setActors(actors);
		
		return movieRepository.save(movie);
		
	}
	
	  public Boolean deleteMovie(Long id) {
		  movieRepository.deleteById(id);
	        return true;
	    }
	  
	  public Movie updateMovie(Long id, String title, String genre, String[] actors) throws Exception {
			Movie movie = movieRepository.findById(id).orElseThrow(() -> new Exception("Not found"));
			movie.setTitle(title);
			movie.setGenre(genre);
			movie.setActors(actors);
			
			return movieRepository.save(movie);
			
		}

}
