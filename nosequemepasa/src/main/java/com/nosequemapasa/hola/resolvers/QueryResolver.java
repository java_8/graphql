package com.nosequemapasa.hola.resolvers;

import java.util.List;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.nosequemapasa.hola.model.Movie;
import com.nosequemapasa.hola.repositories.MovieRepository;






public class QueryResolver implements GraphQLQueryResolver {
	
	private MovieRepository movieRepository;
	
	
	
	public QueryResolver(MovieRepository movieRepository) {
		this.movieRepository = movieRepository;
	}



	public List<Movie> getAllMovies(){
		return movieRepository.findAll();
	}
	

}
